const webpack = require('webpack')
const HtmlWebPackPlugin = require("html-webpack-plugin")
const path = require('path')
const CopyWebpackPlugin = require("copy-webpack-plugin")
const htmlPlugin = new HtmlWebPackPlugin({
    template: "./src/root/index.html",
    filename: "./index.html"
});

module.exports = env => {
    console.log("env==>" + env)
    return {
        entry: ["./src/root"],
        output: {
            path: path.resolve('build'),
            filename: '[hash].bundled.js'
        },
        resolve: {
            extensions: ['*', '.js', '.jsx','.json']
        },
        devServer: {
            proxy: {
                "*": "http://[::1]:8080"//https://github.com/webpack/webpack-dev-server/issues/793
            },
            // publicPath: '/assets'
        },
        module: {
            rules: [
                {
                    test: /\.js$|\.jsx$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: "html-loader"
                        }
                    ]
                },
                {
                    test: /\.css$/,
                    use: [
                        {
                            loader: "style-loader"
                        },
                        {
                            loader: "css-loader",
                            options: {
                                modules: true,
                                importLoaders: 1,
                                // localIdentName: "[name]_[local]_[hash:base64]",
                                localIdentName: "[hash:base64]",
                                sourceMap: true
                            }
                        }
                    ]
                },
                {
                    test: /\.jpe?g$|\.ico$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$|\.json$/,
                    loader: 'file-loader?name=[name].[ext]'  // <-- retain original file name
                }//favicon.ico
                //Rule End
            ]
        },
        plugins: [
            htmlPlugin,
            new CopyWebpackPlugin([{ from: 'src/assets', to: 'assets' }]),
            // new webpack.DefinePlugin({
            //     'process.env.domain': JSON.stringify(env.domain),
            //     'process.env.api': JSON.stringify(env.api),
            // })
        ]
    };
}
