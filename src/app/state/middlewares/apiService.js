//https://www.youtube.com/watch?v=ICSeVLe9Tt4
import { fetch } from "../utils"
const baseUrl = process.env.api || ""
const apiService = () => (next) => (action) => {

    const result = next(action);
    if (!action.meta || !action.meta.async) {
        return result;
    }
    const { path, method = "GET", body, credentials } = action.meta;
    if (!path) {
        throw new Error(`'path' not specified for async action ${action.type}`);
    }
    const url = `${baseUrl}${path}`;
    const options = {
        method: method,
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        body: method !== "GET" ? JSON.stringify(body) : null,
        credentials: credentials
    };
    return fetch(url, options).then((json) => handleResolve(json, action, next), (response) => handleReject(response, action, next))
};

export default apiService;
function handleResolve(json, action, next) {
    next({
        type: `${action.type}_COMPLETED`,
        payload: json,
        meta: action.meta,
    });
    return null
}
function handleReject(response, action, next) {
    console.log(response.json)
    console.log(response)
    next({
        type: `${action.type}_FAILED`,
        payload: response.json !== undefined ? response.json: response,
        meta: action.meta,
    });
}