import Cookies from 'js-cookie'

const sessionManager = () => (next) => (action) => {
    const result = next(action)
    if (!action.meta || !action.meta.initial) {
        console.log("Not a session initial phase, bypass.")
        return result
    }

    const { initial = false, name = '', domain = 'localhost' } = action.meta
    if (initial) {
        console.log("Session initialize.")
        handleInitial(name, domain, action, next)
    }

}

export default sessionManager;

function handleInitial(name, domain, action, next) {
    Cookies.remove(name, { path: '/', domain: domain })
    next({
        type: `${action.type}_COMPLETED`,
        payload: { timestamp: Date.now() },
        meta: action.meta
    })

}