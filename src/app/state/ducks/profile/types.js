
export const GET_PROFILE = "profile/GET_PROFILE";
export const GET_PROFILE_COMPLETED = "profile/GET_PROFILE_COMPLETED";
export const GET_PROFILE_FAILED = "profile/GET_PROFILE_FAILED";