import * as types from "./types";

export const profile = () => ({
    type: types.GET_PROFILE,
    meta: {
        async: true,
        blocking: true,
        path: '/api/user/profile',
        method: 'GET',
        credentials: 'include'
    }
});
