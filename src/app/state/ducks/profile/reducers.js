import { combineReducers } from "redux";
import * as types from "./types";
import { createReducer } from "../../utils";

/* State shape
{
    id: string, 
    name: string ,
    username: string,
    email: string, 
    
}
*/
const INIT = { id: "", name: "", username: "", email: "" }

//Loading
const loadingReducer = createReducer(false)({
    [types.GET_PROFILE]: () => true,
    [types.GET_PROFILE_FAILED]: () => false,
    [types.GET_PROFILE_COMPLETED]: () => false,
});
//Error 
const errorReducer = createReducer(false)({
    [types.GET_PROFILE_COMPLETED]: () => false,
    [types.GET_PROFILE_FAILED]: () => true
});
//Reason
const reasonReducer = createReducer("")({
    [types.GET_PROFILE_COMPLETED]: () => "",
    [types.GET_PROFILE_FAILED]: (state, action) => action.payload.reason
});
//Result
const resultReducer = createReducer(INIT)({
    [types.GET_PROFILE_COMPLETED]: (state, action) => action.payload,
    [types.GET_PROFILE_FAILED]: () => INIT
});

export default combineReducers({
    isLoading: loadingReducer,
    error: errorReducer,
    result: resultReducer,
    reason: reasonReducer,
});