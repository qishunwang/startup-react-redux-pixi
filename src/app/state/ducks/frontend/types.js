export const SELECT_DASHBOARD = "frontend/SELECT_DASHBOARD";
export const AGREEMENT_SITTER = "frontend/AGREEMENT_SITTER";
export const AGREEMENT_WALKER = "frontend/AGREEMENT_WALKER";
export const PREPARED_LEASH_WALKER = "frontend/PREPARED_LEASH_WALKER";