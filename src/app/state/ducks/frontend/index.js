import reducer from "./reducers";

import * as frontendOperations from "./operations";

export {
    frontendOperations,
};

export default reducer;