import * as types from "./types";

export const select = (position) => ({
    type: types.SELECT_DASHBOARD,
    position: position
    
});

export const agreementSitter = (hidden) => ({
    type: types.AGREEMENT_SITTER,
    agreement: {
        sitter: hidden
    }
});

export const agreementWalker = (hidden) => ({
    type: types.AGREEMENT_WALKER,
    agreement: {
        walker: hidden
    }
});

export const preparedLeash = (prepared) => ({
    type: types.PREPARED_LEASH_WALKER,
    walkerForm: {
        preparedLeash: prepared
    }
});
