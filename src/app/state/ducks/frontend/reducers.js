import { combineReducers } from "redux";
import * as types from "./types";
import { createReducer } from "../../utils";
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

/* State shape
{
    position: string, 
    agreement: 
        {
            sitter: bool,
            walker: bool
        },
    walkerForm:
        {
            preparedLeash: bool
        }
}
*/

//frontend dashboard position
const positionReducer = createReducer("")({
    [types.SELECT_DASHBOARD]: (state, action) => action.position
});
//frontend agreement
const agreementReducer = createReducer({ sitter : false, walker : false })({
    [types.AGREEMENT_SITTER]: (state, action) => {
        return { sitter : action.agreement.sitter, walker : state.walker } 
    },
    [types.AGREEMENT_WALKER]: (state, action) => {
        return { sitter : state.sitter, walker : action.agreement.walker } 
}
});
//frontend walker form state
const walkerFormReducer = createReducer({ preparedLeash : true })({
    [types.PREPARED_LEASH_WALKER]: (state, action) => {
        return { preparedLeash : action.walkerForm.preparedLeash} 
    } 
});
const frontendReducer = combineReducers({
    position: positionReducer,
    agreement: agreementReducer,
    walkerForm: walkerFormReducer,
});

const frontendPersistConfig = {
    key: 'frontend',
    storage: storage
}

export default persistReducer(frontendPersistConfig, frontendReducer);

