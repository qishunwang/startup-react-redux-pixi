import { combineReducers } from "redux";
import * as types from "./types";
import { createReducer } from "../../utils";

/* State shape

{
    card: string, 
    payment_type: string ,
    card_type: string, 
    status: string

}
*/
const INIT = { card: "", payment_type: "", card_type: "" ,status: ""}

//Loading
const loadingReducer = createReducer(false)({
    [types.GET_PAYMENT]: () => true,
    [types.GET_PAYMENT_COMPLETED]: () => false,
    [types.GET_PAYMENT_FAILED]: () => false,
    [types.ADD_PAYMENT]: () => true,
    [types.ADD_PAYMENT_COMPLETED]: () => false,
    [types.ADD_PAYMENT_FAILED]: () => false,
    [types.REMOVE_PAYMENT]: () => true,
    [types.REMOVE_PAYMENT_COMPLETED]: () => false,
    [types.REMOVE_PAYMENT_FAILED]: () => false,
});
//Error 
const errorReducer = createReducer(false)({
    [types.GET_PAYMENT_COMPLETED]: () => false,
    [types.GET_PAYMENT_FAILED]: () => true,
    [types.ADD_PAYMENT_COMPLETED]: () => false,
    [types.ADD_PAYMENT_FAILED]: () => true,
    [types.REMOVE_PAYMENT_COMPLETED]: () => false,
    [types.REMOVE_PAYMENT_FAILED]: () => true,
});
//Reason
const reasonReducer = createReducer("")({
    [types.GET_PAYMENT_COMPLETED]: () => "",
    [types.ADD_PAYMENT_COMPLETED]: () => "",
    [types.REMOVE_PAYMENT_COMPLETED]: () => "",
    [types.GET_PAYMENT_FAILED]: (state, action) => action.payload.reason,
    [types.ADD_PAYMENT_FAILED]: (state, action) => action.payload.reason,
    [types.REMOVE_PAYMENT_FAILED]: (state, action) => action.payload.reason,
});
//Result
const resultReducer = createReducer(INIT)({
    [types.GET_PAYMENT_COMPLETED]: (state, action) => action.payload,
    [types.ADD_PAYMENT_COMPLETED]:  (state, action) => action.payload,
    [types.GET_PAYMENT_FAILED]: () => INIT,
    [types.ADD_PAYMENT_FAILED]: () => INIT,
});

//Data
const dataReducer = createReducer([])({
    [types.GET_PAYMENT_COMPLETED]: (state, action) => action.payload.data,
    [types.ADD_PAYMENT_COMPLETED]: (state, action) => {
        return [...state, action.payload]
    },
    [types.REMOVE_PAYMENT_COMPLETED]: (state, action) => {
        return state.filter(payment => payment.card !== action.payload.id)
    }
});

export default combineReducers({
    data: dataReducer,
    isLoading: loadingReducer,
    error: errorReducer,
    result: resultReducer,
    reason: reasonReducer,
});