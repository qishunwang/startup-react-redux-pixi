import * as types from "./types";

export const getPayment = () => ({
    type: types.GET_PAYMENT,
    meta: {
        async: true,
        blocking: true,
        path: '/api/user/payment',
        method: 'GET',
        credentials: 'include'
    }
});

export const addPayment = (token) => ({
    type: types.ADD_PAYMENT,
    meta: {
        async: true,
        blocking: true,
        path: '/api/user/payment',
        method: 'POST',
        body: token,
        credentials: 'include'
    }
});

export const removePayment = (card_id) => ({
    type: types.REMOVE_PAYMENT,
    meta: {
        async: true,
        blocking: true,
        path: '/api/user/payment/' + card_id,
        method: 'DELETE',
        credentials: 'include'
    }
});
