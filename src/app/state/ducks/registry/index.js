import reducer from "./reducers";

import * as registryOperations from "./operations";

export {
    registryOperations,
};

export default reducer;