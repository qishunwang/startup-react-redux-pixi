import * as types from "./types";
//Action Creators
export const userRegister = (username, password, email, name) => ({
    type: types.USER_REGISTER,
    meta: {
        async: true,
        blocking: true,
        path: '/api/user/registry',
        method: 'POST',
        body: {
            username: username,
            password: password,
            email: email,
            name: name
        },
        credentials: 'include'
    }
});
