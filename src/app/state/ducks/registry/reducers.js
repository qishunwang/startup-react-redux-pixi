import { combineReducers } from "redux";
import * as types from "./types";
import { createReducer } from "../../utils";

/* State shape
{
    isLoading: bool,
    error: bool,
    reason: string,
    result: {},
    form: {
        name: string ,
        username: string,
        password: string,
        email: string, 
    }
}
*/
const FORM = { name: "", username: "", password: "", email: "" }
//Registry
const registryReducer = createReducer(FORM)({
    [types.USER_REGISTER]: (state, action) => action.meta.body
});
//Loading
const loadingReducer = createReducer(false)({
    [types.USER_REGISTER]: () => true,
    [types.USER_REGISTER_COMPLETED]: () => false,
    [types.USER_REGISTER_FAILED]: () => false,
});
//Error 
const errorReducer = createReducer(false)({
    [types.USER_REGISTER_COMPLETED]: () => false,
    [types.USER_REGISTER_FAILED]: (state, action) => action.payload.error
});
//Reason
const reasonReducer = createReducer("")({
    [types.USER_REGISTER_COMPLETED]: () => "",
    [types.USER_REGISTER_FAILED]: (state, action) => action.payload.reason
});
//Result
const resultReducer = createReducer(null)({
    [types.USER_REGISTER_COMPLETED]: (state, action) => action.payload,
    [types.USER_REGISTER_FAILED]: () => null
});

export default combineReducers({
    isLoading: loadingReducer,
    error: errorReducer,
    result: resultReducer,
    reason: reasonReducer,
    form: registryReducer,
});