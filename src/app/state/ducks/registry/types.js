//action types
export const USER_REGISTER = "registry/USER_REGISTER";
export const USER_REGISTER_COMPLETED = "registry/USER_REGISTER_COMPLETED";
export const USER_REGISTER_FAILED = "registry/USER_REGISTER_FAILED";