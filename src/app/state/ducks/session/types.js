
export const LOGIN = "session/LOGIN";
export const LOGIN_COMPLETED = "session/LOGIN_COMPLETED";
export const LOGIN_FAILED = "session/LOGIN_FAILED";

export const LOGOUT = "session/LOGOUT";
export const LOGOUT_COMPLETED = "session/LOGOUT_COMPLETED";
export const LOGOUT_FAILED = "session/LOGOUT_FAILED";

export const INITIALIZE = "session/INITIALIZE_SESSION";
export const INITIALIZE_COMPLETED = "session/INITIALIZE_SESSION_COMPLETED";

export const SET_REDIRECT_AFTER_LOGIN = "session/SET_REDIRECT_AFTER_LOGIN";