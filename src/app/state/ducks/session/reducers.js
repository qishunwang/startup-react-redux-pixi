import { combineReducers } from "redux";
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import * as types from "./types";
import { createReducer } from "../../utils";
/* State shape
{
    isLoading: bool,
    isAuthenticated: bool,
    error: bool,
    reason: string,
    result: {},
}
*/

//Loading
const loadingReducer = createReducer(false)({
    [types.LOGIN]: () => true,
    [types.LOGIN_FAILED]: () => false,
    [types.LOGIN_COMPLETED]: () => false,
    [types.LOGOUT]: () => true,
    [types.LOGOUT_FAILED]: () => false,
    [types.LOGOUT_COMPLETED]: () => false,
});
//Auth
const authReducer = createReducer(false)({
    [types.LOGIN]: () => false,
    [types.LOGIN_FAILED]: () => false,
    [types.LOGIN_COMPLETED]: () => true,
    [types.INITIALIZE_COMPLETED]: () => false,
});
//todo after logout completed
const destroySessionReducer = createReducer(null)({
    [types.INITIALIZE_COMPLETED]: (state, action) => action.payload.timestamp,
});
//Error 
const errorReducer = createReducer(false)({
    [types.LOGIN_COMPLETED]: () => false,
    [types.LOGOUT_COMPLETED]: () => false,
    [types.LOGIN_FAILED]: () => true,
    [types.LOGOUT_FAILED]: () => true,
});
//Reason
const reasonReducer = createReducer("")({
    [types.LOGIN_COMPLETED]: () => "",
    [types.LOGIN_FAILED]:  (state, action) => action.payload.reason,
    [types.LOGOUT_COMPLETED]: () => "",
    [types.LOGOUT_FAILED]: (state, action) => action.payload.reason,
});
//Result
const resultReducer = createReducer(null)({
    [types.LOGIN_COMPLETED]: (state, action) => action.payload,
    [types.LOGIN_FAILED]:(state, action) => action.payload,
    [types.LOGOUT_COMPLETED]: (state, action) => action.payload,
    [types.LOGOUT_FAILED]: (state, action) => action.payload,
});

const sessionReducer = combineReducers({
    isLoading: loadingReducer,
    isAuthenticated: authReducer,
    destroySessionAt: destroySessionReducer,
    error: errorReducer,
    reason: reasonReducer,
    result: resultReducer
});

const authPersistConfig = {
    key: 'auth',
    storage: storage
}

export default persistReducer(authPersistConfig, sessionReducer);