import * as types from "./types";

//action creators
export const login = (username, password) => ({
    type: types.LOGIN,
    meta: {
        async: true,
        blocking: true,
        path: '/api/user/login',
        method: 'POST',
        body: {
            username: username,
            password: password
        },
        credentials: 'include'
    }
});

export const logout = () => ({
    type: types.LOGOUT,
    meta: {
        async: true,
        blocking: true,
        method: 'GET',
        path: '/api/user/logout',
        credentials: 'include'
    }
});

export const initializeSession = () => ({
    type: types.INITIALIZE,
    meta: {
        initial: true,
        domain: process.env.domain,
        name: 'vapor-session'
    }
});
