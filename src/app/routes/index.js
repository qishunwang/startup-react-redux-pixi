import {
    Home,
    Playground,
}
    // Login,
    // Registry,
    // Dashboard,
    // Profile
    from "../views/pages";
// import { withAuthentication, lazyLoad } from "../views/enhancers";

const routes = [
    {
        path: "/home",
        component: Home,
        exact: true,
    },
    {
        path: "/",
        component: Playground,
        exact: true,
    },
    // {
    //     path: "/profile",
    //     component: Profile,//withAuthentication( lazyLoad( ( ) => import( "../views/pages/profile" ) ) ),
    //     // exact: true,
    // },
    // {
    //     path: "/dashboard",
    //     component: Dashboard,//withAuthentication( lazyLoad( ( ) => import( "../views/pages/profile" ) ) ),
    //     // exact: true,
    // },
    // {
    //     path: "/login",
    //     component: Login,
    //     exact: true,
    // },
    // {
    //     path: "/registry",
    //     component: Registry,
    //     exact: true,
    // },
];

export default routes;