import React from "react"
import Clock from "../containers/clock"

export default (props) => <Home props={props} />

class Home extends React.Component {
    constructor(props) {
        super(props)
        this.initialState = {
            dateTime: new Date(
                // Use current year
                (new Date()).getFullYear(),
                // Initial state is always May 27th, at noon
                4, 27, 12)
        }
        this.state = this.initialState;
        this.onDateTimeUpdate = this.onDateTimeUpdate.bind(this)
    }
    render() {
        return (
            <React.Fragment>
                <Clock
                    diameter={300}
                    dateTime={this.state.dateTime}
                    onDateTimeUpdate={this.onDateTimeUpdate} 
                    />
            </React.Fragment>
        )
    }
    onDateTimeUpdate(dateTime) {
        this.setState({ dateTime: dateTime })
    }
} 