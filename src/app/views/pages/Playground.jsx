import React from "react"
import Game from "../containers/game"
export default (props) => <Playground props={props} />

class Playground extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <React.Fragment>
                <Game
                    w={1024} h={1024}
                />
            </React.Fragment>
        )
    }
} 