import PropTypes from "prop-types"

const { shape, string } = PropTypes

export default shape({
    username: string.isRequired,
    password: string.isRequired,
});