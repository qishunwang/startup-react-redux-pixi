import React from 'react'

export const higherOrderComponent = (WrappedComponent) => {
  class HOC extends React.Component {
    constructor(props) {
      super(props)
    }
    render() {
      return <WrappedComponent  {...this.props} />
    }
  }
  return HOC;
}