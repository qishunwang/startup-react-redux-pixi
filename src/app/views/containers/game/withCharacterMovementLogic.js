import React from 'react'

export const withCharacterMovementLogic = (WrappedComponent) => {
    class HOC extends React.Component {
        constructor(props) {
            super(props)
            this.character = {}
            this.character.speed = 19
            this.character.friction = 0.98 //Coefficient of friction 0~1
        }
        render() {
            return <WrappedComponent {...this.props}
                character={this.character}
                updateKeyboardArrowOn={this.updateKeyboardArrowOn}
            />
        }
        updateKeyboardArrowOn = (obj, character, keyboard) => {
            let speed = character.speed
            let friction = character.friction
            if (keyboard.isKeyRightPressed) {
                if (obj.vx < speed) {
                    obj.vx++
                }
            }
            if (keyboard.isKeyLeftPressed) {
                if (obj.vx > -speed) {
                    obj.vx--
                }
            }
            if (keyboard.isKeyUpPressed) {
                if (obj.vy > -speed) {
                    obj.vy--
                }
            }
            if (keyboard.isKeyDownPressed) {
                if (obj.vy < speed) {
                    obj.vy++
                }
            }
            obj.vy *= friction
            obj.y += obj.vy
            obj.vx *= friction
            obj.x += obj.vx
        }
    }
    return HOC;
}