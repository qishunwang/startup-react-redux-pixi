import React from 'react'

export const withLoadProgressHandler = (WrappedComponent) => {
    class HOC extends React.Component {
        constructor(props) {
            super(props)
        }
        render() {
            return <WrappedComponent
                {...this.props}
                loadProgressHandler={this.loadProgressHandler}
            />
        }
        loadProgressHandler(loader, resource) {
            //Display the file `url` currently being loaded
            console.log("loading: " + resource.url);
            //Display the percentage of files currently loaded
            console.log("progress: " + loader.progress + "%");
            console.log("error:" + resource.error)
        }
    }
    return HOC;
}