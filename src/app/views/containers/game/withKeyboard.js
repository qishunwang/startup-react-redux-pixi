import React from "react"
import { keyboard } from "../utils"

export const withKeyboard = (WrappedComponent) => {
    class HOC extends React.Component {
        constructor(props) {
            super(props)
            this.keyboard = {}
            this.keyboard.isKeyLeftPressed = false
            this.keyboard.isKeyRightPressed = false
            this.keyboard.isKeyUpPressed = false
            this.keyboard.isKeyDownPressed = false
        }
        render() {
            return <WrappedComponent {...this.props}
                setupArrowMovementOn={this.setupArrowMovementOn}
                removeArrowMovementOn={this.removeArrowMovementOn}
                keyboard={this.keyboard}
            />
        }
        removeArrowMovementOn(obj) {
            obj.keyDown.unsubscribe()
            obj.keyLeft.unsubscribe()
            obj.keyRight.unsubscribe()
            obj.keyUp.unsubscribe()
        }
        setupArrowMovementOn(obj) {
            obj.keyDown = keyboard("ArrowDown")
            obj.keyLeft = keyboard("ArrowLeft")
            obj.keyRight = keyboard("ArrowRight")
            obj.keyUp = keyboard("ArrowUp")
            obj.keyDown.press = () => {
                this.keyboard.isKeyDownPressed = true
            }
            obj.keyDown.release = () => {
                this.keyboard.isKeyDownPressed = false
            }
            obj.keyLeft.press = () => {
                this.keyboard.isKeyLeftPressed = true
            }
            obj.keyLeft.release = () => {
                this.keyboard.isKeyLeftPressed = false
            }
            obj.keyRight.press = () => {
                this.keyboard.isKeyRightPressed = true
            }
            obj.keyRight.release = () => {
                this.keyboard.isKeyRightPressed = false
            }
            obj.keyUp.press = () => {
                this.keyboard.isKeyUpPressed = true
            }
            obj.keyUp.release = () => {
                this.keyboard.isKeyUpPressed = false
            }
        }
    }
    return HOC;
}