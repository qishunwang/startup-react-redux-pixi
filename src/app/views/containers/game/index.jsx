import React from 'react'
import PropTypes from 'prop-types'
import * as PIXI from 'pixi.js'
import styled from 'styled-components'
import { withKeyboard } from './withKeyboard.js'
import { withCharacterMovementLogic } from './withCharacterMovementLogic'
import { withLoadProgressHandler } from './withLoadProgressHandler'

const Container = styled.div`
display: flex;
justify-content: center;
`
const Game = class Game extends React.Component {
    constructor(props) {
        super(props)
        this.loader = new PIXI.loaders.Loader()
        this.setup = this.setup.bind(this)
        this.gameLoop = this.gameLoop.bind(this)
        props.setupArrowMovementOn(this)
    }
    render() {
        return (
            <React.Fragment >
                <Container ref={(el) => { this.gameRef = el }}></Container>
            </React.Fragment>
        )
    }
    componentDidMount() {
        this.game = new PIXI.Application({
            backgroundColor: 0x003300,
            width: this.props.w,
            height: this.props.h,
            sharedLoader: true,
            sharedTicker: true,
            forceCanvas: true
        })
        this.gameRef.appendChild(this.game.view)
        this.loader
            .add('car', 'assets/car.json')
            .on("progress", this.props.loadProgressHandler)
            .load(this.setup)
    }

    setup(loader, resources) {
        console.log("didload")
        this.player = this.createSprite()
        this.game.stage.addChild(this.player)
        this.game.ticker.add(delta => this.gameLoop(delta));
    }

    gameLoop(delta) {
        this.updateSkinOn(this.player)
        this.props.updateKeyboardArrowOn(this.player, this.props.character, this.props.keyboard)
    }

    componentWillUnmount() {
        this.game.stop()
        this.props.removeArrowMovementOn(this)
        console.log("componentWillUnmount")
    }

    createSprite() {
        var sprite = new PIXI.Sprite()
        sprite.position.set(0, 90)
        sprite.vy = 0
        sprite.vx = 0
        sprite.texture = PIXI.utils.TextureCache["car01iso_0005.png"]
        return sprite
    }

    updateSkinOn(obj) {
        //left
        if (obj.vx < -1) {
            console.log(obj.vx)
            obj.texture = PIXI.utils.TextureCache["car01iso_0003.png"]
        }
        //right
        if (obj.vx > 1) {
            obj.texture = PIXI.utils.TextureCache["car01iso_0007.png"]
        }
        //down
        if (obj.vy > 1) {
            obj.texture = PIXI.utils.TextureCache["car01iso_0001.png"]
        }
        //up
        if (obj.vy < -1) {
            obj.texture = PIXI.utils.TextureCache["car01iso_0005.png"]
        }
    }
}

Game.propTypes = {
    w: PropTypes.number.isRequired,
    h: PropTypes.number.isRequired
}

export default withLoadProgressHandler(withCharacterMovementLogic(withKeyboard(Game)))