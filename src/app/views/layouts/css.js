import { createGlobalStyle } from "styled-components";
import  * as themes from "../themes/colors"

export const GlobalStyle = createGlobalStyle`
html, body, #root , #root > div{
    background-color: ${themes.main}
    height: 100%;
    }
    body {
        display: block;
        margin:0px;
        // overflow-y: hidden;
    }
`