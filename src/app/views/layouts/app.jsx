import React from "react";
import { Route } from "react-router-dom";
import routes from "../../routes";
import { GlobalStyle } from "./css";

class App extends React.Component {
	render() {
		return (
			<React.Fragment>
				{routes.map(route => (<Route key={route.path} {...route} />))}
				<GlobalStyle />
			</React.Fragment>
		)
	}
}

export default App