import React from 'react'
import { render } from 'react-dom'
import styles from './index.css'
import { HashRouter as Router } from "react-router-dom"
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore } from 'redux-persist'
import configureStore from "../app/state/store"

import App from "../app/views/layouts/app"

const reduxStore = configureStore(window.REDUX_INITIAL_DATA)

const Root = (
    <div className={styles.root}>
        <Provider store={reduxStore}>
            <PersistGate loading={null} persistor={persistStore(reduxStore)}>
                <Router>
                    <App />
                </Router>
            </PersistGate>
        </Provider>
    </div>
)


render(Root, document.getElementById("root")); 