## Install Dependencies
    yarn
## Run Up Project on local server
    yarn start
## Build Project
    yarn build --env.domain=${DOMAIN} --env.api=${API}
## Support Docker Image
[Dockerfile](#Dockerfile)