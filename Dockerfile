# Stage 1 - the build process
FROM node:alpine as builder

ARG DOMAIN
ARG API

WORKDIR /app
COPY package.json ./
RUN yarn
COPY . ./
RUN yarn build --env.domain=${DOMAIN} --env.api=${API}

# Stage 2 - the production environment
FROM nginx:1.12-alpine
COPY --from=builder /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]